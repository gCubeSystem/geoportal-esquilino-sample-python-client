#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @author: Giancarlo Panichi
# revisited by @author: Francesco Mangiacrapa

# Created on 2023/02/01
#
import requests
import urllib.parse


class IAM_Token_Client:

    def __init__(self, iamURL, the_clientId, the_clientSecret):
        self.iamURL = iamURL
        self.clientId = the_clientId
        self.clientSecret = the_clientSecret
        print('IAM URL: ' + self.iamURL)
        #print('ClientId: ' + self.clientId)
        #print('ClientSecret: ' + self.clientSecret)

    def getAccessToken(self):
        #print("getAccessToken()")

        loginheaders = {'Accept': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded'}
        logindata = {'grant_type': 'client_credentials', 'client_id': self.clientId, 'client_secret': self.clientSecret}

        # Get Access Token by client_id
        resp1 = requests.post(self.iamURL, data=logindata, headers=loginheaders)
        jwt1 = resp1.json()
        #print("Resp AccessToken: ", jwt1)
        accessToken = jwt1["access_token"]
        return accessToken

    def getUmaToken(self, gcube_context):
        print("getUmaToken()")
        context = gcube_context

        #print('Context: ' + context)

        context = urllib.parse.quote(context, safe='')
        #print('Context safe: ' + context)

        accessToken = self.getAccessToken()

        umaheaders = {'Accept': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded'}
        umadata = {'grant_type': 'urn:ietf:params:oauth:grant-type:uma-ticket', 'audience': context}

        # Get UMA token for context
        umaheaders["Authorization"] = "Bearer " + accessToken
        resp2 = requests.post(self.iamURL, data=umadata, headers=umaheaders)
        jwt2 = resp2.json()
        #print("Resp UmaToken: ", jwt2)
        umaToken = jwt2["access_token"]
        return umaToken

    def __str__(self):
        return 'IAMExample'
