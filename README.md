# GeoPortal Esquilino Sample Python Client

The GeoPortal Esquilino sample python client, is a basic python main that shows how to interact with the "Geoportal Service" and register/publish a sample "Esquilino Project"

The `gcube_context` is `/d4science.research-infrastructures.eu/gCubeApps/Esquiline`

The `UCD_ID` is `esquilino`

Test Files are located in the subdir [test_files](https://code-repo.d4science.org/gCubeSystem/geoportal-esquilino-sample-python-client/src/branch/master/test_files)

Test Esquilino dataset that will be registered is [test_dataset_esquilino](https://code-repo.d4science.org/gCubeSystem/geoportal-esquilino-sample-python-client/src/branch/master/test_files/test_dataset_esquilino.json)

## Built With

* [python 3+](https://docs.python.org/)

**Uses**

* python 3+ [https://docs.python.org/3](https://docs.python.org/3)
* D4Science IAM [Identity and Access Management (IAM)](https://dev.d4science.org/authorization/)
* geportal-service API [geportal-service API](https://geoportal.d4science.org/geoportal-service/api-docs/index.html)

## Requires

In the main.py:

* `service_Client_ID` = ''
* `service_Client_Secret` = ''

## Documentation

Service Account [Using Service Accounts](https://dev.d4science.org/using-service-accounts)

gCube CMS Suite DOC [gCube CMS Suite DOC](https://geoportal.d4science.org/geoportal-service/docs/index.html)

## See also

Python example for interaction with D4Science IAM [IAM interaction](https://code-repo.d4science.org/gCubeSystem/iamexample)

## Change log

See the [Releases](https://code-repo.d4science.org/gCubeSystem/geoportal-data-viewer-app/releases)

## Authors

* **Francesco Mangiacrapa** ([ORCID](https://orcid.org/0000-0002-6528-664X)) Computer Scientist at [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes including:

- the Sixth Framework Programme for Research and Technological Development
    - DILIGENT (grant no. 004260).
- the Seventh Framework Programme for research, technological development and demonstration 
    - D4Science (grant no. 212488);
    - D4Science-II (grant no.239019);
    - ENVRI (grant no. 283465);
    - EUBrazilOpenBio (grant no. 288754);
    - iMarine(grant no. 283644).
- the H2020 research and innovation programme 
    - BlueBRIDGE (grant no. 675680);
    - EGIEngage (grant no. 654142);
    - ENVRIplus (grant no. 654182);
    - PARTHENOS (grant no. 654119);
    - SoBigData (grant no. 654024);
    - DESIRA (grant no. 818194);
    - ARIADNEplus (grant no. 823914);
    - RISIS2 (grant no. 824091);
    - PerformFish (grant no. 727610);
    - AGINFRAplus (grant no. 731001).


