#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @author: Francesco Mangiacrapa
#
# Created on 2023/02/16
#

from urllib.request import urlopen


class URL_Reader:

    @staticmethod
    def read_file(link):
        try:
            print("Reading the url: %s" % link)
            return urlopen(link)
        except Exception as e:
            print("Something went wrong %s" % e)
