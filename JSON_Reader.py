#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# @author: Francesco Mangiacrapa
#
# Created on 2023/02/16
#

import json

from URL_Reader_Util import URL_Reader


class JSON_Reader:

    def __init__(self, json_file_url):
        try:
            f = URL_Reader.read_file(json_file_url)
            # returns JSON object as
            # a dictionary
            self.data = json.load(f)
        except Exception as e:
            print("Something went wrong %s" % e)
        finally:
            f.close()

    @staticmethod
    def pretty_print_json(json_data, indent=2):
        print("Json Data: \n%s" % json.dumps(json_data, indent=indent))

    @staticmethod
    def to_json(text):
        return json.loads(text)

    def __str__(self):
        return 'JSON_Reader'
